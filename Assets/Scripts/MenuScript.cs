﻿using UnityEngine;
using System.Collections;

public class MenuScript : MonoBehaviour {


	public void Update()
	{

		/*if (Input.anyKeyDown) {
			Debug.Log("OnClickStart");
			CameraFade.StartAlphaFade( Color.black, false, 0.75f, 0f, () => { Application.LoadLevel("GUI Story"); } );//Load next scene
				}*/

	}

	public void OnClickStart(){
		Debug.Log("OnClickStart");
		CameraFade.StartAlphaFade( Color.black, false, 0.75f, 0f, () => { Application.LoadLevel("GUI Story"); } );//Load next scene
	}

	public void OnClickMenu(){
		Debug.Log("OnClickMenu");
		CameraFade.StartAlphaFade( Color.black, false, 0.75f, 0f, () => { Application.LoadLevel("GUI Main Menu"); } );//Load next scene
	}
	
	public void OnClickCredits(){
		Debug.Log("OnClickCredits");
		CameraFade.StartAlphaFade( Color.black, false, 0.75f, 0f, () => { Application.LoadLevel("GUI Credits"); } );//Load next scene
	}
	
	public void OnClickTutorial(){
		//StartAlphaFade(Color newScreenOverlayColor, bool isFadeIn, float fadeDuration )
		CameraFade.StartAlphaFade( Color.black, false, 0.75f, 0f, () => { Application.LoadLevel("GUI Tutorial 1"); } );//Load next scene
	}

	public void OnClickTutorial2(){
		//StartAlphaFade(Color newScreenOverlayColor, bool isFadeIn, float fadeDuration )
		CameraFade.StartAlphaFade( Color.black, false, 0.75f, 0f, () => { Application.LoadLevel("GUI Tutorial 1b"); } );//Load next scene
	}
	
}
