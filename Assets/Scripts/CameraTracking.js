﻿#pragma strict
var Car : GameObject;
var Speed : float;
var SpeedIncrease : float;
var Sensitivity : float;
var MaxPosition = 4.33352;
var Offset : float;
var dummyTran: Vector3;
function Start () 
{
//s	Tracking();
}

function Update () 
{
dummyTran=this.transform.position;
dummyTran.x = Car.transform.position.x*0.66;
this.transform.localRotation.z = Car.transform.rotation.y*-0.12;
this.transform.position = Vector3.Lerp(this.transform.position, dummyTran, 15*Time.deltaTime);
}

function Tracking()
{
	while(true)
	{
		if(Car.transform.position.x<(this.transform.position.x) && this.transform.position.x>-MaxPosition)
		{
			var CarPosition = Car.transform.position.x;
			
			if(Car.transform.position.x<=-MaxPosition)
			CarPosition = -MaxPosition;
			
			
			var Distance = this.transform.position.x-CarPosition;
			
			Speed=(Distance+Offset)/Sensitivity;
			this.transform.position.x-=Speed;
		
		}
		else
		if(Car.transform.position.x>(this.transform.position.x) && this.transform.position.x<MaxPosition)
		{
			var CarPosition2 = Car.transform.position.x;
			
			if(Car.transform.position.x>=MaxPosition)
			CarPosition2 = MaxPosition;
			
			
			var Distance2 = CarPosition2 - this.transform.position.x;
			
			Speed=(Distance2+Offset)/Sensitivity;
			this.transform.position.x+=Speed;
		
		}
		
		if(Car.transform.position.x>this.transform.position.x-0.1 && Car.transform.position.x<this.transform.position.x+0.1)
		this.transform.position.x=Car.transform.position.x;
		
		yield;
	}

}