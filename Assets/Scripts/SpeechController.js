﻿#pragma strict
var Question : String[];
var GoodAnswer : String[];
var BadAnswer : String[];
var CorrectAnswer : int[];
var SpeechBubble : GameObject;
var SpeechBubble2 : GameObject;
var SpeechBubbleText : GameObject;
var SpeechBubbleText2 : GameObject;
var Car : GameObject;
var HeartBarObject : GameObject;

var StartOfSwipe : Vector2;
var EndOfSwipe : Vector2;
var DifferenceOfSwipe : Vector2;

var QuestionBacking : GameObject;

var UsedSpeech = 0;

var Difficulty = 2;

var ButtonPress : GameObject[];

var Positive : int[];
var Negative : int[];

var ActiveQTE = 0;

var PositiveFail = false;
var NegativeFail=false;

var Control = false;

var Fail1 : GameObject;
var Fail2 : GameObject;
var Answer = 0;

var HeartPassIncrease : float;
var HeartFailDecrease : float;

var Timer = 5f;
var TimerBar : GameObject;

var TimerDrainAmount = 0.022;

var HeartFX : GameObject;
var AngryFX : GameObject;


var Heart : GameObject;
var AngryFaces : GameObject;
var Swiping = false;

function SwipeUp()
{

	if(PositiveFail==false && Positive[ActiveQTE] == 0)
			 	{
			 		ButtonPress[ActiveQTE].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE].GetComponent(IconSelection).frames[4];
			 		NegativeFail=true;
			 		Fail2.SetActive(true);
			 		ActiveQTE++;
			 		
			 		if(ActiveQTE == Difficulty)
			 		{
			 			Answer = 1;
			 			CheckAnswer();
			 			Control=false;
			 		}
			 	}
			 	else
			 	if(NegativeFail==false && Negative[ActiveQTE] == 0)
			 	{
			 		ButtonPress[ActiveQTE+5].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE+5].GetComponent(IconSelection).frames[4];
			 		PositiveFail=true;
			 		Fail1.SetActive(true);
			 		ActiveQTE++;
			 		
			 		if(ActiveQTE == Difficulty)
			 		{
			 			Answer = 2;
			 			print("ANSWER = " + Answer);
			 			CheckAnswer();
			 			Control=false;
			 		}
			 	}
			 	else
			 	{
			 		Control = false;
			 		Fail1.SetActive(true);
			 		Fail2.SetActive(true);
			 		print("You Fail");
			 		Failure();
			 	}
	    	
		    	
		    	
			

}

function SwipeRight()
{

	if(PositiveFail==false && Positive[ActiveQTE] == 1)
		 	{
		 		ButtonPress[ActiveQTE].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE].GetComponent(IconSelection).frames[4];
		 		NegativeFail=true;
		 		Fail2.SetActive(true);
		 		ActiveQTE++;
		 		
		 		if(ActiveQTE == Difficulty)
		 		{
		 			Answer = 1;
		 			CheckAnswer();
		 			Control=false;
		 		}
		 	}
		 	else
		 	if(NegativeFail==false && Negative[ActiveQTE] == 1)
		 	{
		 		ButtonPress[ActiveQTE+5].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE+5].GetComponent(IconSelection).frames[4];
		 		PositiveFail=true;
		 		Fail1.SetActive(true);
		 		ActiveQTE++;
		 		
		 		if(ActiveQTE == Difficulty)
		 		{
		 			Answer = 2;
		 			print("ANSWER = " + Answer);
		 			CheckAnswer();
		 			Control=false;
		 		}
		 	}
		 	else
		 	{
		 		Control = false;
		 		Fail1.SetActive(true);
		 		Fail2.SetActive(true);
		 		print("You Fail");
		 		Failure();
		 	}
    	
    	

}

function SwipeDown()
{

	if(PositiveFail==false && Positive[ActiveQTE] == 2)
		 	{
		 		ButtonPress[ActiveQTE].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE].GetComponent(IconSelection).frames[4];
		 		NegativeFail=true;
		 		Fail2.SetActive(true);
		 		ActiveQTE++;
		 		
		 		if(ActiveQTE == Difficulty)
		 		{
		 			Answer = 1;
		 			CheckAnswer();
		 			Control=false;
		 		}
		 	}
		 	else
		 	if(NegativeFail==false && Negative[ActiveQTE] == 2)
		 	{
		 		ButtonPress[ActiveQTE+5].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE+5].GetComponent(IconSelection).frames[4];
		 		PositiveFail=true;
		 		Fail1.SetActive(true);
		 		ActiveQTE++;
		 		
		 		if(ActiveQTE == Difficulty)
		 		{
		 			Answer = 2;
		 			print("ANSWER = " + Answer);
		 			CheckAnswer();
		 			Control=false;
		 		}
		 	}
		 	else
		 	{
		 		Control = false;
		 		Fail1.SetActive(true);
		 		Fail2.SetActive(true);
		 		print("You Fail");
		 		Failure();
		 	}

}

function SwipeLeft()
{

	if(PositiveFail==false && Positive[ActiveQTE] == 3)
		 	{
		 		ButtonPress[ActiveQTE].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE].GetComponent(IconSelection).frames[4];
		 		NegativeFail=true;
		 		Fail2.SetActive(true);
		 		ActiveQTE++;
		 		
		 		if(ActiveQTE == Difficulty)
		 		{
		 			Answer = 1;
		 			CheckAnswer();
		 			Control=false;
		 		}
		 	}
		 	else
		 	if(NegativeFail==false && Negative[ActiveQTE] == 3)
		 	{
		 		ButtonPress[ActiveQTE+5].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE+5].GetComponent(IconSelection).frames[4];
		 		PositiveFail=true;
		 		Fail1.SetActive(true);
		 		ActiveQTE++;
		 		
		 		if(ActiveQTE == Difficulty)
		 		{
		 			Answer = 2;
		 			print("ANSWER = " + Answer);
		 			CheckAnswer();
		 			Control=false;
		 		}
		 	}
		 	else
		 	{
		 		Control = false;
		 		Fail1.SetActive(true);
		 		Fail2.SetActive(true);
		 		print("You Fail");
		 		Failure();
		 	}

}

function Awake()
{
	//put all text here "123456789123456789123456789"
	Question[0] = "You still love me, right?";
	GoodAnswer[0] = "Even more than I love\narmed robbery";
	BadAnswer[0] = "'Love' is such a strong word...";
	
	Question[1] = "Do you still trust me?";
	GoodAnswer[1] = "Ofcourse I do";
	BadAnswer[1] = "You remember dropping me\n in that trust exercise?";
	
	Question[2] = "Are we going to regret\n doing this?";
	BadAnswer[2] = "Almost as much as I\n regret getting engaged.";
	GoodAnswer[2] = "It made me feel alive.";
	
	Question[3] = "Do you think I’m a bad person?";
	BadAnswer[3] = "You literally enjoy \nstealing candy from babies.";
	GoodAnswer[3] = "You just do what you\n need to to get by.";
	
	Question[4] = "Do you want a\n bigger share of the money?";
	BadAnswer[4] = "I’m taking what I deserve.";
	GoodAnswer[4] = "It’s 50/ 50 all the way.";
	
	Question[5] = "Do you think we’re going \nto get out of this alive?";
	GoodAnswer[5] = "I know we are.";
	BadAnswer[5] = "Let’s just take things \none breath at a time.";
	
	Question[6] = "On the run! \nIsn't this exciting?";
	GoodAnswer[6] = "We have the wind in \nour hair and a trunk full \nof cash!";
	BadAnswer[6] = "As exciting as swimming\n with hungry sharks";
		
	Question[7] = "Will being on the run \n strain our relationship?";
	BadAnswer[7] = "Agreeing what to eat for\n dinner is enough to strain\n our relationship";
	GoodAnswer[7] = "We’re strong enough \nto survive anything. ";
	
	Question[8] = "Do you still want\n to marry me?";
	GoodAnswer[8] = "We can use this money to\n have the most amazing wedding";
	BadAnswer[8] = "Implying I wanted\n to get married in \nthe first place?";

	Question[9] = "Robbing banks is \nkinda hot... isn’t it?";
	GoodAnswer[9] = "As hot as \nhandcuffs at night.";
	BadAnswer[9] = "Is there something \nwrong with you?";
	
	Question[10] = "Isn’t this the most\n fun you’ve ever had?";
	GoodAnswer[10] = "It’s such a buzz.";
	BadAnswer[10] = "Fun isn’t the word I would use";
	
	Question[11] = "Do you feel differently\n about me now?";
	BadAnswer[11] = "I feel you need to be quiet...";
	GoodAnswer[11] = "Of course not.";
	
	Question[12] = "So... you wanna do \nthis again sometime?";
	GoodAnswer[12] = "No doubt about it.";
	BadAnswer[12] = "No. This has been a \ndisaster start to finish.";
	
	Question[13] = "Can we buy a house now?";
	GoodAnswer[13] = "Just one? \nHow about a mansion?";
	BadAnswer[13] = "Let’s just focus on \ngetting away alive. ";
	
	Question[14] = "Did I do a good job today?";
	GoodAnswer[14] = "You did great.";
	BadAnswer[14] = "You’re just... the worst.";
		
	Question[15] = "I’m a pretty awful \ncriminal aren’t I?";
	BadAnswer[15] = "You’re pretty reckless.";
	GoodAnswer[15] = "You did your best.";
	
	Question[16] = "I’m sorry I took my balaclava off\n back there. Can you forgive me?";
	GoodAnswer[16] = "It’s done now,\n don’t worry about it.";
	BadAnswer[16] = "What the hell \nwere you thinking?";

	Question[17] = "Have you ever wanted \nto cheat on me?";
	BadAnswer[17] = "You mean so far today?";
	GoodAnswer[17] = "You’re the only one for me.";

	Question[18] = "This was a mistake, wasn’t it?";
	BadAnswer[18] = "Not our brightest idea";
	GoodAnswer[18] = "Not if we don’t get caught.";

	Question[19] = "Are you still insisting\n on investing the money\n in stocks and shares?";
	GoodAnswer[19] = "We have to plan \nfor our retirement.";
	BadAnswer[19] = "Let’s go to Vegas!";

	Question[20] = "Can we buy an island?";
	GoodAnswer[20] = "Let’s buy three!";
	BadAnswer[20] = "We robbed one bank. \nWe’re not supervillans.";

	Question[21] = "Can we buy a McLaren F1?";
	GoodAnswer[21] = "Only the best for you.";
	BadAnswer[21] = "Let’s see how much money\n we have first.";

	Question[22] = "You'd do anything for me, \nwouldn't you?";
	GoodAnswer[22] = "Anything.";
	BadAnswer[22] = "I'd lie to you, \nand that's the truth";

	Question[23] = "You wanna go on holiday?";
	GoodAnswer[23] = "Yeah, would be good to get\n out the country for a while";
	BadAnswer[23] = "I’d rather not go through\n customs anytime soon";

	Question[24] = "Are you ok? You seem distant.";
	GoodAnswer[24] = "Yeah, sorry. \nJust a little pre-occupied.";
	BadAnswer[24] = "We are on the run from \nthe police. Do you really need \nto ask that question? ";

	Question[25] = "Have you got something\n on your mind?";
	BadAnswer[25] = "Erm, just... you know. \nEscaping from the police.";
	GoodAnswer[25] = "Just keeping my mind \non the road, honey bun.";

}


function Start () 
{
	StartLoop();
}


/*function Scrolling()
{
	
	// Get movement of the finger since last frame
		var touchDeltaPosition:Vector2 = Input.GetTouch(0).deltaPosition;
		
		// Move object across XY plane
		if(touchDeltaPosition.y >30 && Control)
    	{
    	
    		if(PositiveFail==false && Positive[ActiveQTE] == 0)
		 	{
		 		ButtonPress[ActiveQTE].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE].GetComponent(IconSelection).frames[4];
		 		NegativeFail=true;
		 		Fail2.SetActive(true);
		 		ActiveQTE++;
		 		
		 		if(ActiveQTE == Difficulty)
		 		{
		 			Answer = 1;
		 			CheckAnswer();
		 			Control=false;
		 		}
		 	}
		 	else
		 	if(NegativeFail==false && Negative[ActiveQTE] == 0)
		 	{
		 		ButtonPress[ActiveQTE+5].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE+5].GetComponent(IconSelection).frames[4];
		 		PositiveFail=true;
		 		Fail1.SetActive(true);
		 		ActiveQTE++;
		 		
		 		if(ActiveQTE == Difficulty)
		 		{
		 			Answer = 2;
		 			print("ANSWER = " + Answer);
		 			CheckAnswer();
		 			Control=false;
		 		}
		 	}
		 	else
		 	{
		 		Control = false;
		 		Fail1.SetActive(true);
		 		Fail2.SetActive(true);
		 		print("You Fail");
		 		Failure();
		 	}
    	
    	}
    	else
    	if(touchDeltaPosition.x >30 && Control)
    	{
    	
    		if(PositiveFail==false && Positive[ActiveQTE] == 1)
		 	{
		 		ButtonPress[ActiveQTE].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE].GetComponent(IconSelection).frames[4];
		 		NegativeFail=true;
		 		Fail2.SetActive(true);
		 		ActiveQTE++;
		 		
		 		if(ActiveQTE == Difficulty)
		 		{
		 			Answer = 1;
		 			CheckAnswer();
		 			Control=false;
		 		}
		 	}
		 	else
		 	if(NegativeFail==false && Negative[ActiveQTE] == 1)
		 	{
		 		ButtonPress[ActiveQTE+5].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE+5].GetComponent(IconSelection).frames[4];
		 		PositiveFail=true;
		 		Fail1.SetActive(true);
		 		ActiveQTE++;
		 		
		 		if(ActiveQTE == Difficulty)
		 		{
		 			Answer = 2;
		 			print("ANSWER = " + Answer);
		 			CheckAnswer();
		 			Control=false;
		 		}
		 	}
		 	else
		 	{
		 		Control = false;
		 		Fail1.SetActive(true);
		 		Fail2.SetActive(true);
		 		print("You Fail");
		 		Failure();
		 	}
    	
    	}
    	else
    	if(touchDeltaPosition.y <-30 && Control)
    	{
    	
    		if(PositiveFail==false && Positive[ActiveQTE] == 2)
		 	{
		 		ButtonPress[ActiveQTE].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE].GetComponent(IconSelection).frames[4];
		 		NegativeFail=true;
		 		Fail2.SetActive(true);
		 		ActiveQTE++;
		 		
		 		if(ActiveQTE == Difficulty)
		 		{
		 			Answer = 1;
		 			CheckAnswer();
		 			Control=false;
		 		}
		 	}
		 	else
		 	if(NegativeFail==false && Negative[ActiveQTE] == 2)
		 	{
		 		ButtonPress[ActiveQTE+5].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE+5].GetComponent(IconSelection).frames[4];
		 		PositiveFail=true;
		 		Fail1.SetActive(true);
		 		ActiveQTE++;
		 		
		 		if(ActiveQTE == Difficulty)
		 		{
		 			Answer = 2;
		 			print("ANSWER = " + Answer);
		 			CheckAnswer();
		 			Control=false;
		 		}
		 	}
		 	else
		 	{
		 		Control = false;
		 		Fail1.SetActive(true);
		 		Fail2.SetActive(true);
		 		print("You Fail");
		 		Failure();
		 	}
    	
    	}
    	else
    	if(touchDeltaPosition.x <-30 && Control)
    	{
    	
    		if(PositiveFail==false && Positive[ActiveQTE] == 3)
		 	{
		 		ButtonPress[ActiveQTE].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE].GetComponent(IconSelection).frames[4];
		 		NegativeFail=true;
		 		Fail2.SetActive(true);
		 		ActiveQTE++;
		 		
		 		if(ActiveQTE == Difficulty)
		 		{
		 			Answer = 1;
		 			CheckAnswer();
		 			Control=false;
		 		}
		 	}
		 	else
		 	if(NegativeFail==false && Negative[ActiveQTE] == 3)
		 	{
		 		ButtonPress[ActiveQTE+5].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE+5].GetComponent(IconSelection).frames[4];
		 		PositiveFail=true;
		 		Fail1.SetActive(true);
		 		ActiveQTE++;
		 		
		 		if(ActiveQTE == Difficulty)
		 		{
		 			Answer = 2;
		 			print("ANSWER = " + Answer);
		 			CheckAnswer();
		 			Control=false;
		 		}
		 	}
		 	else
		 	{
		 		Control = false;
		 		Fail1.SetActive(true);
		 		Fail2.SetActive(true);
		 		print("You Fail");
		 		Failure();
		 	}
    	
    	}
    	
    	

}*/

function Update () 
{
	//iOS Controls
	
	for (var touch : Touch in Input.touches) 
	{
		if (touch.phase == TouchPhase.Ended) 
		{
			if(touch.deltaPosition.y>2 && touch.deltaPosition.x<touch.deltaPosition.y && -touch.deltaPosition.y<touch.deltaPosition.x && Control)
			{
				if(PositiveFail==false && Positive[ActiveQTE] == 0)
			 	{
			 		ButtonPress[ActiveQTE].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE].GetComponent(IconSelection).frames[4];
			 		NegativeFail=true;
			 		Fail2.SetActive(true);
			 		ActiveQTE++;
			 		
			 		if(ActiveQTE == Difficulty)
			 		{
			 			Answer = 1;
			 			CheckAnswer();
			 			Control=false;
			 		}
			 	}
			 	else
			 	if(NegativeFail==false && Negative[ActiveQTE] == 0)
			 	{
			 		ButtonPress[ActiveQTE+5].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE+5].GetComponent(IconSelection).frames[4];
			 		PositiveFail=true;
			 		Fail1.SetActive(true);
			 		ActiveQTE++;
			 		
			 		if(ActiveQTE == Difficulty)
			 		{
			 			Answer = 2;
			 			print("ANSWER = " + Answer);
			 			CheckAnswer();
			 			Control=false;
			 		}
			 	}
			 	else
			 	{
			 		Control = false;
			 		Fail1.SetActive(true);
			 		Fail2.SetActive(true);
			 		print("You Fail");
			 		Failure();
			 	}
	    	
		    	
		    	
			}
			else
			if(touch.deltaPosition.x>2 && touch.deltaPosition.y<touch.deltaPosition.x && -touch.deltaPosition.x<touch.deltaPosition.y && Control)
			{
				if(PositiveFail==false && Positive[ActiveQTE] == 1)
			 	{
			 		ButtonPress[ActiveQTE].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE].GetComponent(IconSelection).frames[4];
			 		NegativeFail=true;
			 		Fail2.SetActive(true);
			 		ActiveQTE++;
			 		
			 		if(ActiveQTE == Difficulty)
			 		{
			 			Answer = 1;
			 			CheckAnswer();
			 			Control=false;
			 		}
			 	}
			 	else
			 	if(NegativeFail==false && Negative[ActiveQTE] == 1)
			 	{
			 		ButtonPress[ActiveQTE+5].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE+5].GetComponent(IconSelection).frames[4];
			 		PositiveFail=true;
			 		Fail1.SetActive(true);
			 		ActiveQTE++;
			 		
			 		if(ActiveQTE == Difficulty)
			 		{
			 			Answer = 2;
			 			print("ANSWER = " + Answer);
			 			CheckAnswer();
			 			Control=false;
			 		}
			 	}
			 	else
			 	{
			 		Control = false;
			 		Fail1.SetActive(true);
			 		Fail2.SetActive(true);
			 		print("You Fail");
			 		Failure();
			 	}
			}
			else
			if(touch.deltaPosition.y<-2 && touch.deltaPosition.x>touch.deltaPosition.y && -touch.deltaPosition.y>-touch.deltaPosition.x && Control)
			{
				if(PositiveFail==false && Positive[ActiveQTE] == 2)
			 	{
			 		ButtonPress[ActiveQTE].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE].GetComponent(IconSelection).frames[4];
			 		NegativeFail=true;
			 		Fail2.SetActive(true);
			 		ActiveQTE++;
			 		
			 		if(ActiveQTE == Difficulty)
			 		{
			 			Answer = 1;
			 			CheckAnswer();
			 			Control=false;
			 		}
			 	}
			 	else
			 	if(NegativeFail==false && Negative[ActiveQTE] == 2)
			 	{
			 		ButtonPress[ActiveQTE+5].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE+5].GetComponent(IconSelection).frames[4];
			 		PositiveFail=true;
			 		Fail1.SetActive(true);
			 		ActiveQTE++;
			 		
			 		if(ActiveQTE == Difficulty)
			 		{
			 			Answer = 2;
			 			print("ANSWER = " + Answer);
			 			CheckAnswer();
			 			Control=false;
			 		}
			 	}
			 	else
			 	{
			 		Control = false;
			 		Fail1.SetActive(true);
			 		Fail2.SetActive(true);
			 		print("You Fail");
			 		Failure();
			 	}
			}
			else
			if(touch.deltaPosition.x<-2 && touch.deltaPosition.y>touch.deltaPosition.x && -touch.deltaPosition.x>-touch.deltaPosition.y && Control)
			{
				if(PositiveFail==false && Positive[ActiveQTE] == 3)
			 	{
			 		ButtonPress[ActiveQTE].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE].GetComponent(IconSelection).frames[4];
			 		NegativeFail=true;
			 		Fail2.SetActive(true);
			 		ActiveQTE++;
			 		
			 		if(ActiveQTE == Difficulty)
			 		{
			 			Answer = 1;
			 			CheckAnswer();
			 			Control=false;
			 		}
			 	}
			 	else
			 	if(NegativeFail==false && Negative[ActiveQTE] == 3)
			 	{
			 		ButtonPress[ActiveQTE+5].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE+5].GetComponent(IconSelection).frames[4];
			 		PositiveFail=true;
			 		Fail1.SetActive(true);
			 		ActiveQTE++;
			 		
			 		if(ActiveQTE == Difficulty)
			 		{
			 			Answer = 2;
			 			print("ANSWER = " + Answer);
			 			CheckAnswer();
			 			Control=false;
			 		}
			 	}
			 	else
			 	{
			 		Control = false;
			 		Fail1.SetActive(true);
			 		Fail2.SetActive(true);
			 		print("You Fail");
			 		Failure();
			 	}
			}
		}
	}
	
	
	
	//ANDROID CONTROLS
	/*if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved && !Swiping) 
	{
		Swiping=true;
		Scrolling();
	}
	
	if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended && Swiping==true)
	{
		Swiping = false;
	}
	
	//END OF ANDROID CONTROLS
	*/
	
	//NEW METHOD
	/*
	if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began) 
	{
		StartOfSwipe = Input.GetTouch(0).deltaPosition;
	}
	
	if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended) 
	{
		EndOfSwipe = Input.GetTouch(0).deltaPosition;
		DifferenceOfSwipe = StartOfSwipe - EndOfSwipe;
		if(DifferenceOfSwipe.y>1 && Control)
		SwipeUp();
		else
		if(DifferenceOfSwipe.y<-1 && Control)
		SwipeDown();
		else
		if(DifferenceOfSwipe.x>1 && Control)
		SwipeRight();
		else
		if(DifferenceOfSwipe.x<-1 && Control)
		SwipeLeft();
		
	}
	*/
	
	 if (Input.GetKeyUp("up") && Control)
	 {
	 	if(PositiveFail==false && Positive[ActiveQTE] == 0)
	 	{
	 		ButtonPress[ActiveQTE].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE].GetComponent(IconSelection).frames[4];
	 		NegativeFail=true;
	 		Fail2.SetActive(true);
	 		ActiveQTE++;
	 		
	 		if(ActiveQTE == Difficulty)
	 		{
	 			Answer = 1;
	 			CheckAnswer();
	 			Control=false;
	 		}
	 	}
	 	else
	 	if(NegativeFail==false && Negative[ActiveQTE] == 0)
	 	{
	 		ButtonPress[ActiveQTE+5].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE+5].GetComponent(IconSelection).frames[4];
	 		PositiveFail=true;
	 		Fail1.SetActive(true);
	 		ActiveQTE++;
	 		
	 		if(ActiveQTE == Difficulty)
	 		{
	 			Answer = 2;
	 			print("ANSWER = " + Answer);
	 			CheckAnswer();
	 			Control=false;
	 		}
	 	}
	 	else
	 	{
	 		Control = false;
	 		Fail1.SetActive(true);
	 		Fail2.SetActive(true);
	 		print("You Fail");
	 		Failure();
	 	}
	 }
	 
	 
	 
	 if (Input.GetKeyUp("right") && Control)
	 {
	 	if(PositiveFail==false && Positive[ActiveQTE] == 1)
	 	{
	 		ButtonPress[ActiveQTE].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE].GetComponent(IconSelection).frames[4];
	 		NegativeFail=true;
	 		Fail2.SetActive(true);
	 		ActiveQTE++;
	 		
	 		if(ActiveQTE == Difficulty)
	 		{
	 			Answer = 1;
	 			CheckAnswer();
	 			Control=false;
	 		}
	 	}
	 	else
	 	if(NegativeFail==false && Negative[ActiveQTE] == 1)
	 	{
	 		ButtonPress[ActiveQTE+5].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE+5].GetComponent(IconSelection).frames[4];
	 		PositiveFail=true;
	 		Fail1.SetActive(true);
	 		ActiveQTE++;
	 		
	 		if(ActiveQTE == Difficulty)
	 		{
	 			Answer = 2;
	 			print("ANSWER = " + Answer);
	 			CheckAnswer();
	 			Control=false;
	 		}
	 	}
	 	else
	 	{
	 		Control = false;
	 		Fail1.SetActive(true);
	 		Fail2.SetActive(true);
	 		print("You Fail");
	 		Failure();
	 	}
	 }
	 
	 if (Input.GetKeyUp("down") && Control)
	 {
	 	if(PositiveFail==false && Positive[ActiveQTE] == 2)
	 	{
	 		ButtonPress[ActiveQTE].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE].GetComponent(IconSelection).frames[4];
	 		NegativeFail=true;
	 		Fail2.SetActive(true);
	 		ActiveQTE++;
	 		
	 		if(ActiveQTE == Difficulty)
	 		{
	 			Answer = 1;
	 			CheckAnswer();
	 			Control=false;
	 		}
	 	}
	 	else
	 	if(NegativeFail==false && Negative[ActiveQTE] == 2)
	 	{
	 		ButtonPress[ActiveQTE+5].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE+5].GetComponent(IconSelection).frames[4];
	 		PositiveFail=true;
	 		Fail1.SetActive(true);
	 		ActiveQTE++;
	 		
	 		if(ActiveQTE == Difficulty)
	 		{
	 			Answer = 2;
	 			print("ANSWER = " + Answer);
	 			CheckAnswer();
	 			Control=false;
	 		}
	 	}
	 	else
	 	{
	 		Control = false;
	 		Fail1.SetActive(true);
	 		Fail2.SetActive(true);
	 		print("You Fail");
	 		Failure();
	 	}
	 }
	 
	 if (Input.GetKeyUp("left") && Control)
	 {
	 	if(PositiveFail==false && Positive[ActiveQTE] == 3)
	 	{
	 		ButtonPress[ActiveQTE].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE].GetComponent(IconSelection).frames[4];
	 		NegativeFail=true;
	 		Fail2.SetActive(true);
	 		ActiveQTE++;
	 		
	 		if(ActiveQTE == Difficulty)
	 		{
	 			Answer = 1;
	 			CheckAnswer();
	 			Control=false;
	 		}
	 	}
	 	else
	 	if(NegativeFail==false && Negative[ActiveQTE] == 3)
	 	{
	 		ButtonPress[ActiveQTE+5].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[ActiveQTE+5].GetComponent(IconSelection).frames[4];
	 		PositiveFail=true;
	 		Fail1.SetActive(true);
	 		ActiveQTE++;
	 		
	 		if(ActiveQTE == Difficulty)
	 		{
	 			Answer = 2;
	 			print("ANSWER = " + Answer);
	 			CheckAnswer();
	 			Control=false;
	 		}
	 	}
	 	else
	 	{
	 		Control = false;
	 		Fail1.SetActive(true);
	 		Fail2.SetActive(true);
	 		print("You Fail");
	 		Failure();
	 	}
	 }
	 
	 
}

function SpeechTimer()
{

	Timer = 5f;
	
	while(Timer>0f&& Control)
	{
	
		Timer-=TimerDrainAmount;
		TimerBar.transform.localScale.x = Timer/1.42;
		yield;
	
	}
	
	if(Control)
	{
		Control = false;
		Fail1.SetActive(true);
		Fail2.SetActive(true);
		print("You Fail");
		Failure();
	}
	
	

}

function ResetAll()
{
	QuestionBacking.SetActive(false);
	TimerBar.SetActive(false);
	Timer = 5;
	NegativeFail = false;
	PositiveFail = false;
	Fail1.SetActive(false);
	Fail2.SetActive(false);
	ButtonPress[0].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[0].GetComponent(IconSelection).frames[5];
	ButtonPress[1].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[1].GetComponent(IconSelection).frames[5];
	ButtonPress[2].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[2].GetComponent(IconSelection).frames[5];
	ButtonPress[3].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[3].GetComponent(IconSelection).frames[5];
	ButtonPress[4].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[4].GetComponent(IconSelection).frames[5];
	ButtonPress[5].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[5].GetComponent(IconSelection).frames[5];
	ButtonPress[6].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[6].GetComponent(IconSelection).frames[5];
	ButtonPress[7].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[7].GetComponent(IconSelection).frames[5];
	ButtonPress[8].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[8].GetComponent(IconSelection).frames[5];
	ButtonPress[9].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[9].GetComponent(IconSelection).frames[5];
	ActiveQTE=0;
	
	yield WaitForSeconds(2);
	ScaleDown();
}

function Failure()
{
	ResetAll();
	
	SpeechBubbleText2.GetComponent(TextMesh).text = ". . . ";
	ScaleUp2();
	yield WaitForSeconds(4);
	ScaleDown2();
	AngryFX.GetComponent(ParticleSystem).Play();
	AngryFaces.GetComponent.<AudioSource>().Play();
	if(Car.GetComponent(Controller).HeartBar >HeartFailDecrease)
	{
		Car.GetComponent(Controller).HeartBar-=HeartFailDecrease;
	}
	else
	{
		Car.GetComponent(Controller).HeartBar=0.1;
	}
	HeartBarObject.transform.localScale.x = ((Car.GetComponent(Controller).HeartBar*2.5)/100);
	StartLoop();

}

function CheckAnswer()
{
	ResetAll();
	print("Answer = " +Answer);
	print("Correct Answer = " +CorrectAnswer[UsedSpeech]);
	print(UsedSpeech);
	if(Answer == CorrectAnswer[UsedSpeech])
	{
		SpeechBubbleText2.GetComponent(TextMesh).text = GoodAnswer[UsedSpeech];
		ScaleUp2();
		yield WaitForSeconds(4);
		ScaleDown2();
		
		HeartFX.GetComponent(ParticleSystem).Play();
		Heart.GetComponent.<AudioSource>().Play();
		print("GOOD");
		if(Car.GetComponent(Controller).HeartBar>0)
		{
			Car.GetComponent(Controller).HeartBar+=HeartPassIncrease;
			if(Car.GetComponent(Controller).HeartBar > 100)
			Car.GetComponent(Controller).HeartBar = 100;
			HeartBarObject.transform.localScale.x = ((Car.GetComponent(Controller).HeartBar*2.5)/100);
		}
		StartLoop();

	}
	else
	{
		SpeechBubbleText2.GetComponent(TextMesh).text = BadAnswer[UsedSpeech];
		ScaleUp2();
		yield WaitForSeconds(4);
		ScaleDown2();
		
		print("BAD(ish)");
		StartLoop();
	}

}


function StartLoop()
{

	Car.GetComponent(Controller).QTEHappening=false;
	yield WaitForSeconds(5);

	if(!Car.GetComponent(Controller).PullOverHappening && Car.GetComponent(Controller).Travelling)
	{
		Car.GetComponent(Controller).QTEHappening=true;
		Conversation();
	}

}

function Conversation()
{
	print("TRIGGERING USEDSPEECH");
	UsedSpeech = Random.Range(0,Question.Length-UsedSpeech);
	//UsedSpeech = 25;
	SpeechBubbleText.GetComponent(TextMesh).text = Question[UsedSpeech];
	ScaleUp();
	BringInUI();

}

function BringInUI()
{
	yield WaitForSeconds(3);
	QuestionBacking.SetActive(true);
	yield WaitForSeconds(1);
	for(var x = 0 ; x < Difficulty ; x++)
	{
		Positive[x] = Random.Range(0,4);

		
		if(Positive[x] == 3)
		{
			Negative[x] = 0;
		}
		else
		{
			Negative[x] = Positive[x]+1;
		}

		
		
		ButtonPress[x].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[x].GetComponent(IconSelection).frames[Positive[x]];
		ButtonPress[x+5].GetComponent(UnityEngine.UI.Image).sprite = ButtonPress[x+5].GetComponent(IconSelection).frames[Negative[x]];
		
		
		ButtonPress[x].SetActive(true);
		ButtonPress[x+5].SetActive(true);
	}
	Control=true;
	TimerBar.SetActive(true);
	SpeechTimer();
	

}


function ScaleDown()
{

	iTween.ScaleTo(SpeechBubble.gameObject, iTween.Hash("scale", Vector3(0,0,0), "time", 0.5, "easetype", iTween.EaseType.easeInOutSine));
	

}

function ScaleUp()
{

	iTween.ScaleTo(SpeechBubble.gameObject, iTween.Hash("scale", Vector3(2.385394,3.0291,1), "time", 0.5, "easetype", iTween.EaseType.easeInOutSine));
	

}

function ScaleDown2()
{

	iTween.ScaleTo(SpeechBubble2.gameObject, iTween.Hash("scale", Vector3(0,0,0), "time", 0.5, "easetype", iTween.EaseType.easeInOutSine));
	

}

function ScaleUp2()
{

	iTween.ScaleTo(SpeechBubble2.gameObject, iTween.Hash("scale", Vector3(2.385394,3.0291,1), "time", 0.5, "easetype", iTween.EaseType.easeInOutSine));
	

}