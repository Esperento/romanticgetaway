﻿#pragma strict

function Awake () 
{
	Application.targetFrameRate = 60;
	this.GetComponent(SpriteRenderer).color.a = 0f;
	Splash();
}

function Update () 
{

}

function Splash()
{
	yield WaitForSeconds(0.5);
	while(this.GetComponent(SpriteRenderer).color.a <1f)
	{
		this.GetComponent(SpriteRenderer).color.a += 0.02f;
		yield;
	}
	
	yield WaitForSeconds(2);
	
	while(this.GetComponent(SpriteRenderer).color.a >0f)
	{
		this.GetComponent(SpriteRenderer).color.a -= 0.02f;
		yield;
	}
	
	yield WaitForSeconds(0.4);

	Application.LoadLevel ("GUI Main Menu");
}