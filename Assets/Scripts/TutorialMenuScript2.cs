﻿using UnityEngine;
using System.Collections;

public class TutorialMenuScript2 : MonoBehaviour {
	
	public void Update()
	{
		if(Input.anyKey)
		{

			Debug.Log("OnClickScreen");
			CameraFade.StartAlphaFade( Color.black, false, 0.75f, 0f, () => { Application.LoadLevel("GUI Main Menu"); } );//Load next scene
		}
	}
}
