﻿#pragma strict
var Speed : float;
var CounterAcceleration : float;
var TopSpeed : float;
var Acceleration : float;
var SlowDown : float;
var AgainstWall=false;
var AgainstWallLeft=false;
var AgainstWallRight=false;
var TurningLeft=false;
var TurningRight=false;
var Control = true;
var BounceAmount : float;
var BounceWaitTime : float;
var RotateSpeed = 0.01;
var DrivingSpeed : float;
var MaxWidth : float;
var BounceOff : float;

var Lights : GameObject;

var HeartBar : float;

var PlayerDistance : float;
var CopDistance : float;

var HeartBarObject : GameObject;

var HeartBarDecreaseAmount : float;

var QTEHappening = false;

var PullOverHappening = false;

var SpeechBubble : GameObject;
var SpeechBubbleText : GameObject;

var MoneyVFX: GameObject;

var SpaceSpam = false;

var Sorry : GameObject[];

var SorryTally = 0;

var Hurry : GameObject;

var TempDrivingSpeed : float;

var SpeechControllerObject : GameObject;

var PlayerArrow : GameObject;
var CopArrow : GameObject;
var DistanceBar : GameObject;

var DistancePerSecond : float;

var Travelling = true;

var Boost = 0;

var LightSource : GameObject;

var CreditsObject : GameObject;

var Lost = false;

var SnapBack = false;

var Busted : GameObject;

var BustedPoint : GameObject;
var BustedPoint2 : GameObject;

var BlackBackground : GameObject;

var Won = false;

var CrateHit : GameObject;
var AngryFaces : GameObject;
var CarCrash : GameObject;
var Engine : GameObject;
var Heart : GameObject;
var SorryAudio : GameObject;

var TapToContinue = false;

var CreditsMusic : GameObject;

var MaxRotation : float;
var MinRotation : float;


function Start () 
{
	HeartBarDecrease();
	TravelDistance();
	StartSnapBack();
	
}

function StartSnapBack()
{

	yield WaitForSeconds(1);
	SnapBack=true;

}

function Update () 
{
		if(!Travelling || Lost)
		Control=false;
		
		if(SnapBack)
		{
			if(this.transform.position.z<=Camera.main.transform.position.z+2.5)
			this.transform.position.z=Camera.main.transform.position.z+2.5;
		}
		
		if(this.transform.position.x>MaxWidth)
		this.transform.position.x=MaxWidth;
		
		if(this.transform.position.x<-MaxWidth)
		this.transform.position.x=-MaxWidth;
		
		this.transform.position.z+=((DrivingSpeed - BounceOff)+ Boost);
		
		if(CopDistance>PlayerDistance && !Lost)
		{
			Lost = true;
			
			Lose();
		}
		
		if(BounceOff > 0)
		{
			BounceOff-=0.035;
		}
		else
		if(this.transform.position.z-Camera.main.transform.position.z<7 && Travelling)
		{
			this.transform.position.z+=0.01;
		}
		else
		if(BounceOff <=0)
		{
			BounceOff=0;
		}
		
		
		if(this.transform.position.z>-8)
		Camera.main.transform.position.z+=DrivingSpeed;
	
	
	if(TapToContinue && Input.anyKeyDown)
	{
		Application.LoadLevel("GUI Main Menu");
	}
	
	if (Input.anyKeyDown && PullOverHappening && SpaceSpam)
	{
		ScaleDown();
		if(SorryTally>3)
		Hurry.SetActive(false);
		Sorry[SorryTally].SetActive(true);
		SorryAudio.GetComponent.<AudioSource>().Play();
		SorryTally++;
		if(SorryTally == 14)
		{
			DriveAgain();
		}
	
	}
	 
	 
	 if ((Input.GetKey("left") || Input.acceleration.x<-0.1) && Control && !AgainstWallLeft)
	 {
	 	TurningLeft=true;
	 	if(this.transform.rotation.y >0 && !AgainstWall)
	 	{
	 		print(this.transform.localRotation.y);
	 		this.transform.rotation.y-=RotateSpeed*2;
	 	}
	 	else
	 	if(this.transform.rotation.y >-0.15 && !AgainstWall)
	 	{
	 		this.transform.rotation.y-=RotateSpeed;
	 	}
	 	
	 	
	 	if(Speed>0)
	 	{
	 		Speed-=CounterAcceleration;
	 	}
	 	else
	 	if(Speed>-TopSpeed)
	 	{
	 		Speed -=Acceleration;
	 	}
	 	
	 }
	 else
	 if ((Input.GetKey("right") || Input.acceleration.x>0.1) && Control && !AgainstWallRight)
	 {
	 	TurningRight=true;
	 	
	 	if(this.transform.rotation.y <0 && !AgainstWall)
	 	{
	 		this.transform.rotation.y+=RotateSpeed*2;
	 		if(this.transform.rotation.y>-(RotateSpeed/2) && this.transform.rotation.y<(RotateSpeed/2))
	 		this.transform.rotation.y=0;
	 	}
	 	else
	 	if(this.transform.rotation.y <0.15 && !AgainstWall)
	 	{
	 		this.transform.rotation.y+=RotateSpeed;
	 		if(this.transform.rotation.y>-(RotateSpeed/2) && this.transform.rotation.y<(RotateSpeed/2))
	 		this.transform.rotation.y=0;
	 	}
	 	
	 	
	 	if(Speed<0)
	 	{
	 		Speed+=CounterAcceleration;
	 	}
	 	else
	 	if(Speed<TopSpeed)
	 	{
	 		Speed +=Acceleration;
	 	}
	 	
	 }
	 else
	 if(Speed>0)
	 {
	 	
	 	Speed -=SlowDown;
	 	if(Speed>-0.1 && Speed < 0.1)
		Speed=0;
	 	if(this.transform.rotation.y<0)
	 	{
	 		this.transform.rotation.y+=RotateSpeed;
	 		if(this.transform.rotation.y>-(RotateSpeed/2) && this.transform.rotation.y<(RotateSpeed/2))
	 		this.transform.rotation.y=0;
	 	}
	 }
	 else
	 if(Speed<0)
	 {
	 	Speed +=SlowDown;
	 	if(Speed>-0.1 && Speed < 0.1)
		Speed=0;
	 	if(this.transform.rotation.y>0)
	 	{
	 		this.transform.rotation.y-=RotateSpeed;
	 		if(this.transform.rotation.y>-(RotateSpeed/2) && this.transform.rotation.y<(RotateSpeed/2))
	 		this.transform.rotation.y=0;
	 	}
	 }
	 
	 if (Input.acceleration.x<0.09)
	 {
	 	TurningRight = false;
	 }
	 
	 if (Input.acceleration.x>-0.09)
	 {
	 	TurningLeft = false;
	 }
	 
	 if(TurningLeft == false && TurningRight == false && this.transform.rotation.y!=0)
	 {
	 
	 	if(this.transform.rotation.y<0)
	 	{
	 		this.transform.rotation.y+=RotateSpeed;
	 		if(this.transform.rotation.y>-(RotateSpeed/2) && this.transform.rotation.y<(RotateSpeed/2))
	 		this.transform.rotation.y=0;
	 	}
	 	else
	 	if(this.transform.rotation.y>0)
	 	{
	 		this.transform.rotation.y-=RotateSpeed;
	 		if(this.transform.rotation.y>-(RotateSpeed/2) && this.transform.rotation.y<(RotateSpeed/2))
	 		this.transform.rotation.y=0;
	 	}
	 
	 }

	 if(Input.acceleration.x>0.1)
	 this.transform.position.x+=(Speed * Input.acceleration.x);
	 else
	 if(Input.acceleration.x<0.1)
	 this.transform.position.x+=(Speed * (-Input.acceleration.x));
	 

}


function TravelDistance()
{

	while(Travelling)
	{
		if(!PullOverHappening)
		{
			PlayerDistance+=DistancePerSecond;
			PlayerArrow.transform.localPosition.y = ((DistanceBar.transform.localPosition.y - (DistanceBar.gameObject.GetComponent.<Renderer>().bounds.size.y/2))+PlayerDistance);
		}
		CopDistance+=DistancePerSecond;
		CopArrow.transform.localPosition.y = ((DistanceBar.transform.localPosition.y - (DistanceBar.gameObject.GetComponent.<Renderer>().bounds.size.y/2))+CopDistance);
		
		
		if(PlayerDistance>=4.2)
		{
			Travelling=false;
			Win();
		}
		yield;
	
	}

}

function Win()
{
	if(!Lost)
	{
		Camera.main.GetComponent.<AudioSource>().Stop();
		CreditsMusic.GetComponent.<AudioSource>().Play();
		Won=true;
		CreditsObject.GetComponent(CreditsSimon).Scrolling();
		Control=false;
		DistanceBar.SetActive(false);
		CopArrow.SetActive(false);
		PlayerArrow.SetActive(false);
		Lights.SetActive(false);
		Boost = 1;
		EndGame();
		while(LightSource.GetComponent.<Light>().intensity > -10)
		{
			LightSource.GetComponent.<Light>().intensity -=0.003;
			yield;
		}
		
		
	}
	

}

function EndGame()
{

	yield WaitForSeconds(6.5);
	TapToContinue = true;

}

function Lose()
{
	if(!Won)
	{
		print("LOSTLOSTLOST");
		Lost = true;
		Control=false;
		DistanceBar.SetActive(false);
		CopArrow.SetActive(false);
		PlayerArrow.SetActive(false);
		MoneyVFX.GetComponent(ParticleSystem).Stop();
		TempDrivingSpeed = DrivingSpeed;
		while(DrivingSpeed>0)
		{
			DrivingSpeed-=0.02;
			yield;
		}
		DrivingSpeed=0;
		iTween.MoveTo(Busted.gameObject, iTween.Hash("position", BustedPoint.transform, "time", 0.2, "easetype", iTween.EaseType.linear));
		yield WaitForSeconds(2);
		
		while(BlackBackground.gameObject.GetComponent(SpriteRenderer).color.a <1f)
		{
			BlackBackground.gameObject.GetComponent(SpriteRenderer).color.a +=0.01;
			yield;
		}
		yield WaitForSeconds(1);
		iTween.MoveTo(Busted.gameObject, iTween.Hash("position", BustedPoint2.transform, "time", 0.2, "easetype", iTween.EaseType.linear));
		yield WaitForSeconds(0.3);
		GameObject.Find("AdsObject").GetComponent(Ads).GameOver();
		
		//Application.LoadLevel("GUI Game Over");
	}
}

function HeartBarDecrease()
{

	while(HeartBar > 0)
	{
		
		HeartBar-=HeartBarDecreaseAmount;
		HeartBarObject.transform.localScale.x = ((HeartBar*2.5)/100);
		yield;
		
	}
	
	while(QTEHappening)
	{
		yield;
	}
	
	PullOver();
	PullOverHappening = true;
}


function PullOver()
{
	SpeechBubbleText.GetComponent(TextMesh).text = "THAT'S IT! PULL\nTHIS CAR OVER\n NOW!!!!";
	ScaleUp();
	yield WaitForSeconds(3);
	Control = false;
	MoneyVFX.GetComponent(ParticleSystem).Stop();
	TempDrivingSpeed = DrivingSpeed;
	while(DrivingSpeed>0)
	{
		DrivingSpeed-=0.02;
		yield;
	}
	Hurry.SetActive(true);
	DrivingSpeed=0;
	SpaceSpam = true;

}

function DriveAgain()
{
	PullOverHappening = false;
	SpaceSpam = false;
	for(var x = 0 ; x < 14 ; x++)
	{
		Sorry[x].SetActive(false);
	}
	SorryTally=0;
	SpeechBubbleText.GetComponent(TextMesh).text = "OK.  I\nforgive you.";
	ScaleUp();
	yield WaitForSeconds(1.5);
	HeartBar = 100;
	Engine.GetComponent.<AudioSource>().Play();
	while(DrivingSpeed<TempDrivingSpeed)
	{
	
		DrivingSpeed+=0.01;
		yield;
	
	}
	Control = true;
	MoneyVFX.GetComponent(ParticleSystem).Play();
	ScaleDown();
	SpeechControllerObject.GetComponent(SpeechController).StartLoop();
	HeartBarDecrease();
}

function OnTriggerEnter(other: Collider) 
{

	if(other.tag == "Wall")
	{
		CarCrash.GetComponent.<AudioSource>().Play();
		/*if(this.transform.position.x<0)
		{
			this.transform.position.x=-MaxWidth;
		}
		else
		{
			this.transform.position.x=MaxWidth;
		}*/
		
		if(Speed <0)
		{
			
			BounceWaitTime = (Speed+(2 * -Speed))/3;
			
		}
		else
		{
			BounceWaitTime = Speed/3;
		}
		
		
		Speed -= (Speed*BounceAmount);
		Control=false;
		GetControlBack();
	}
	

}

function OnTriggerStay(other: Collider) 
{

	if(other.tag == "Wall")
	{
		if(this.transform.position.x>0)
		{
			AgainstWallRight = true;
			
		}
		else
		{
			AgainstWallLeft = true;
	
		}
		
		
		AgainstWall=true;
	}

}

function OnTriggerExit(other: Collider) 
{

	if(other.tag == "Wall")
	{
		
		AgainstWallRight = false;
	
		AgainstWallLeft = false;
		
		AgainstWall=false;
		
	}

}

function OnCollisionEnter(collision : Collision) 
{

	print(collision.gameObject.tag);
	if(collision.gameObject.tag == "Crate" )
	{
		CrateHit.GetComponent.<AudioSource>().Play();
	}
	
	if(collision.gameObject.tag == "AICar" && collision.gameObject.GetComponent(AICar).Invincible==false)
	{
		CarCrash.GetComponent.<AudioSource>().Play();
		PlayerDistance-=0.05;
		collision.gameObject.GetComponent.<Rigidbody>().constraints = RigidbodyConstraints.None;
		collision.gameObject.GetComponent.<Rigidbody>().isKinematic = false;
		collision.gameObject.GetComponent(AICar).Child.SetActive(false);
		collision.gameObject.GetComponent.<BoxCollider>().isTrigger=true;
		if(this.transform.position.x>collision.gameObject.transform.position.x)
		{
			collision.gameObject.GetComponent(Rigidbody).AddForce(Vector3(-600,400,500));
			collision.gameObject.GetComponent(Rigidbody).AddTorque (Vector3(20,10,400));
		}
		else
		if(this.transform.position.x<=collision.gameObject.transform.position.x)
		{
			collision.gameObject.GetComponent(Rigidbody).AddForce(Vector3(600,400,500));
			collision.gameObject.GetComponent(Rigidbody).AddTorque (Vector3(20,10,-400));
		}
		
		if(Speed <0)
		{
			
			BounceWaitTime = (Speed+(2 * -Speed))/3;
			
		}
		else
		{
			BounceWaitTime = Speed/3;
		}
		
		
		Speed -= (Speed*BounceAmount);
		Control=false;
		GetControlBack();
	
	}
	else
	if(collision.gameObject.tag == "AICarBack")
	{
		CarCrash.GetComponent.<AudioSource>().Play();
		PlayerDistance-=0.08;
		BounceOff = 0.5;
		
		collision.gameObject.transform.parent.gameObject.GetComponent(AICar).Invincible=true;
		collision.gameObject.transform.parent.gameObject.GetComponent(AICar).Speed*=4;
		collision.gameObject.transform.parent.gameObject.GetComponent(AICar).Hit();
	}
		
}
function GetControlBack()
{

	yield WaitForSeconds(BounceWaitTime);
	Control=true;

}

function ScaleDown()
{

	iTween.ScaleTo(SpeechBubble.gameObject, iTween.Hash("scale", Vector3(0,0,0), "time", 0.5, "easetype", iTween.EaseType.easeInOutSine));
	

}

function ScaleUp()
{

	iTween.ScaleTo(SpeechBubble.gameObject, iTween.Hash("scale", Vector3(2.385394,3.0291,1), "time", 0.5, "easetype", iTween.EaseType.easeInOutSine));
	

}
