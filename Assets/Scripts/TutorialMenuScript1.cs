﻿using UnityEngine;
using System.Collections;

public class TutorialMenuScript1 : MonoBehaviour {
	
	public void Update()
	{
		if(Input.anyKey)
		{
			Debug.Log("OnClickScreen");
			CameraFade.StartAlphaFade( Color.black, false, 0.75f, 0f, () => { Application.LoadLevel("GUI Tutorial 2"); } );//Load next scene
		}
	}
}
