﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;
using System;
public class Ads : MonoBehaviour {
	InterstitialAd interstitial;
	// Use this for initialization
	void Start () {

		RequestInterstitial ();
		
		
	}


	private void RequestInterstitial () {
		#if UNITY_ANDROID
		string adUnitId = "ca-app-pub-6851071248524790/3042317864";
		#elif UNITY_IPHONE
		string adUnitId = "ca-app-pub-6851071248524790/5452557465";
		#else
		string adUnitId = "Full Page Ad";
		#endif

		// Initialize an InterstitialAd.
		interstitial = new InterstitialAd(adUnitId);
		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder().Build();
		// Load the interstitial with the request.
		interstitial.LoadAd(request);

		
	}

	public void GameOver()
	{

		if (interstitial.IsLoaded ()) 
		{
			interstitial.Show();
		}

		interstitial.AdClosed += HandleAdClosed;
		interstitial.AdOpened += HandleAdClosed;
		interstitial.AdFailedToLoad += HandleAdClosed;


	}

	public void HandleAdClosed(object sender, EventArgs args)
	{
		StartCoroutine("DoSomething");


	}

	IEnumerator DoSomething()
	{
		interstitial.Destroy ();
		yield return new WaitForSeconds(1f);
		Application.LoadLevel("GUI Game Over");
	}

	
	// Update is called once per frame
	void Update () {

	
	}
	



}
